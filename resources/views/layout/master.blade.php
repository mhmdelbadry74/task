@include('layout.partial.header')
@include('layout.partial.navbar')
{{--@include('layout.partial.sidebar')--}}
<!-- BEGIN: Content-->
<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="header-navbar-shadow"></div>
    <div class="content-wrapper">
        @yield('breadcrumb')
        <div class="content-header row">
        </div>
        <div class="content-body">
            <section id="dashboard-ecommerce">
                @yield('content')
            </section>
        </div>
    </div>
</div>
@include('layout.partial.footer')
