
    <div class="sidenav-overlay"></div>
    <div class="drag-target"></div>




    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/switchery/0.8.2/switchery.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>


    <script src="{{asset('admin/active.js')}}"></script>
    <script src="{{asset('admin/assets/js/flatpickr.js')}}"></script>
    <script src="{{asset('admin/app-assets/vendors/js/vendors.min.js')}}"></script>
    <script src="{{asset('admin/app-assets/js/core/app-menu.js')}}"></script>
    <script src="{{asset('admin/app-assets/js/core/app.js')}}"></script>
    <script src="{{asset('admin/app-assets/js/scripts/components.js')}}"></script>
    <script src="{{asset('admin/app-assets/vendors/js/extensions/toastr.min.js')}}"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <script>
        // input date js
        var $list = $(":input[type='date']");

        $(document).ready(function () {
            $(".select2").select2();
        });
    </script>
    <script>
        // input date js
        var $list = $(":input[type='date']");
        $(window).on('load', function () {
            if ($($list).length > 0) {
                $(document).find($list).addClass("custom-input-date");
                $(document).find($list).parents(".controls").addClass("parent-input-date");
                $($list).prop("type", "text");
                flatpickr($list, {
                    disableMobile: true,
                    // minDate: "today",
                });
            }
        })
    </script>

    @yield('js')

    <x-admin.alert />
    {{-- <x-socket /> --}}
</body>
</html>
