<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Country;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    //

    public function index()
    {
        $countries = Country::paginate();
        return response()->json($countries);
    }
}
