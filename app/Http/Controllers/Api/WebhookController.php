<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class WebhookController extends Controller
{
    //

    public function handle(Request $request)
    {
        // Retrieve data from the request
        $data = $request->all();

        // Process the data or send it to the callback API
        $this->sendToCallbackApi($data);

        // Optionally, you can log the webhook request
        // Log::info('Webhook request processed: ' . json_encode($data));

        return response()->json(['status' => 'success']);
    }

    private function sendToCallbackApi($data)
    {
        // Replace 'YOUR_CALLBACK_API_URL' with the actual URL of your callback API
        $callbackApiUrl = 'YOUR_CALLBACK_API_URL';

        // Make a POST request to the callback API
        Http::post($callbackApiUrl, $data);
    }
}
