<?php

namespace App\Http\Controllers;

use App\Http\Requests\Country\StoreRequest;
use App\Http\Requests\Country\UpdateRequest;
use App\Models\Country;
use App\Traits\Report;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    //
    public function index($id = null)
    {
        if (request()->ajax()) {
            $countries = Country::search(request()->searchArray)->latest()->paginate(30);
            $html = view('countries.table' ,compact('countries'))->render() ;
            return response()->json(['html' => $html]);
        }
        return view('countries.index');
    }

    public function create()
    {
        return view('countries.create');
    }


    public function store(StoreRequest $request)
    {
        Country::create($request->validated());
        Report::addToLog('  اضافه بلد') ;
        return response()->json(['url' => route('countries.index')]);
    }

    public function edit($id)
    {
        $country = Country::findOrFail($id);
        return view('countries.edit' , ['country' => $country]);
    }

    public function update(UpdateRequest $request, $id)
    {
        $country = Country::findOrFail($id)->update($request->validated());
        Report::addToLog('  تعديل بلد') ;
        return response()->json(['url' => route('countries.index')]);
    }

    public function show(Country $country)
    {
        $country = Country::findOrFail($country);
        return view('countries.show' , ['country' => $country]);
    }

    public function destroy($id)
    {
        $country = Country::findOrFail($id)->delete();
        Report::addToLog('  حذف بلد') ;
        return response()->json(['id' =>$id]);
    }

    public function destroyAll(Request $request)
    {
        $requestIds = json_decode($request->data);

        foreach ($requestIds as $id) {
            $ids[] = $id->id;
        }
        if (Country::whereIntegerInRaw('id',$ids)->get()->each->delete()) {
            Report::addToLog('  حذف العديد من البلاد') ;
            return response()->json('success');
        } else {
            return response()->json('failed');
        }
    }
}
